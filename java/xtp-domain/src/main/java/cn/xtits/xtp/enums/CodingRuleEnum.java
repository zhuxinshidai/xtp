package cn.xtits.xtp.enums;

/**
 * @fileName: CodingRuleEnum
 * @author: Dan
 * @createDate: 2018-06-08 10:17.
 * @description: 编码规则枚举
 */
public enum CodingRuleEnum {

    ACCESSORY_CODE(1, "AccessoryCodeRule", "配件编码"),

    SUPPLIER_CODE(2, "SupplierCodeRule", "供应商编码"),

    CUSTOM_CODE(3, "CustomCodeRule", "客户编码");

    CodingRuleEnum(Integer code, String key, String name) {
        this.code = code;
        this.key = key;
        this.name = name;
    }

    public Integer code;

    public String key;

    public String name;

}
