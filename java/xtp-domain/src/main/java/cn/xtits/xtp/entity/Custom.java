package cn.xtits.xtp.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class Custom implements Serializable {
    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column bs_custom.Id
     *
     * @mbg.generated
     */
    private Integer id;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column bs_custom.Code
     *
     * @mbg.generated
     */
    private String code;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column bs_custom.Name
     *
     * @mbg.generated
     */
    private String name;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column bs_custom.ShortName
     *
     * @mbg.generated
     */
    private String shortName;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column bs_custom.Director
     *
     * @mbg.generated
     */
    private String director;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column bs_custom.Tel
     *
     * @mbg.generated
     */
    private String tel;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column bs_custom.Phone
     *
     * @mbg.generated
     */
    private String phone;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column bs_custom.Address
     *
     * @mbg.generated
     */
    private String address;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column bs_custom.TaxId
     *
     * @mbg.generated
     */
    private String taxId;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column bs_custom.Mail
     *
     * @mbg.generated
     */
    private String mail;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column bs_custom.Site
     *
     * @mbg.generated
     */
    private String site;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column bs_custom.Fax
     *
     * @mbg.generated
     */
    private String fax;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column bs_custom.Remark
     *
     * @mbg.generated
     */
    private String remark;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column bs_custom.OperatorId
     *
     * @mbg.generated
     */
    private Integer operatorId;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column bs_custom.OperatorName
     *
     * @mbg.generated
     */
    private String operatorName;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column bs_custom.SalesManId
     *
     * @mbg.generated
     */
    private Integer salesManId;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column bs_custom.SalesManName
     *
     * @mbg.generated
     */
    private String salesManName;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column bs_custom.AreaId
     *
     * @mbg.generated
     */
    private Integer areaId;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column bs_custom.AreaCode
     *
     * @mbg.generated
     */
    private String areaCode;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column bs_custom.AreaName
     *
     * @mbg.generated
     */
    private String areaName;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column bs_custom.YearBalance
     *
     * @mbg.generated
     */
    private BigDecimal yearBalance;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column bs_custom.PayDay
     *
     * @mbg.generated
     */
    private Integer payDay;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column bs_custom.Post
     *
     * @mbg.generated
     */
    private String post;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column bs_custom.PayMode
     *
     * @mbg.generated
     */
    private String payMode;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column bs_custom.CreateDate
     *
     * @mbg.generated
     */
    private Date createDate;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column bs_custom.MakeBillMan
     *
     * @mbg.generated
     */
    private String makeBillMan;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column bs_custom.ModifyDate
     *
     * @mbg.generated
     */
    private Date modifyDate;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column bs_custom.Modifier
     *
     * @mbg.generated
     */
    private String modifier;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column bs_custom.DeleteFlag
     *
     * @mbg.generated
     */
    private Boolean deleteFlag;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table bs_custom
     *
     * @mbg.generated
     */
    private static final long serialVersionUID = 1L;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column bs_custom.Id
     *
     * @return the value of bs_custom.Id
     *
     * @mbg.generated
     */
    public Integer getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column bs_custom.Id
     *
     * @param id the value for bs_custom.Id
     *
     * @mbg.generated
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column bs_custom.Code
     *
     * @return the value of bs_custom.Code
     *
     * @mbg.generated
     */
    public String getCode() {
        return code;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column bs_custom.Code
     *
     * @param code the value for bs_custom.Code
     *
     * @mbg.generated
     */
    public void setCode(String code) {
        this.code = code == null ? null : code.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column bs_custom.Name
     *
     * @return the value of bs_custom.Name
     *
     * @mbg.generated
     */
    public String getName() {
        return name;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column bs_custom.Name
     *
     * @param name the value for bs_custom.Name
     *
     * @mbg.generated
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column bs_custom.ShortName
     *
     * @return the value of bs_custom.ShortName
     *
     * @mbg.generated
     */
    public String getShortName() {
        return shortName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column bs_custom.ShortName
     *
     * @param shortName the value for bs_custom.ShortName
     *
     * @mbg.generated
     */
    public void setShortName(String shortName) {
        this.shortName = shortName == null ? null : shortName.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column bs_custom.Director
     *
     * @return the value of bs_custom.Director
     *
     * @mbg.generated
     */
    public String getDirector() {
        return director;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column bs_custom.Director
     *
     * @param director the value for bs_custom.Director
     *
     * @mbg.generated
     */
    public void setDirector(String director) {
        this.director = director == null ? null : director.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column bs_custom.Tel
     *
     * @return the value of bs_custom.Tel
     *
     * @mbg.generated
     */
    public String getTel() {
        return tel;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column bs_custom.Tel
     *
     * @param tel the value for bs_custom.Tel
     *
     * @mbg.generated
     */
    public void setTel(String tel) {
        this.tel = tel == null ? null : tel.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column bs_custom.Phone
     *
     * @return the value of bs_custom.Phone
     *
     * @mbg.generated
     */
    public String getPhone() {
        return phone;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column bs_custom.Phone
     *
     * @param phone the value for bs_custom.Phone
     *
     * @mbg.generated
     */
    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column bs_custom.Address
     *
     * @return the value of bs_custom.Address
     *
     * @mbg.generated
     */
    public String getAddress() {
        return address;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column bs_custom.Address
     *
     * @param address the value for bs_custom.Address
     *
     * @mbg.generated
     */
    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column bs_custom.TaxId
     *
     * @return the value of bs_custom.TaxId
     *
     * @mbg.generated
     */
    public String getTaxId() {
        return taxId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column bs_custom.TaxId
     *
     * @param taxId the value for bs_custom.TaxId
     *
     * @mbg.generated
     */
    public void setTaxId(String taxId) {
        this.taxId = taxId == null ? null : taxId.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column bs_custom.Mail
     *
     * @return the value of bs_custom.Mail
     *
     * @mbg.generated
     */
    public String getMail() {
        return mail;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column bs_custom.Mail
     *
     * @param mail the value for bs_custom.Mail
     *
     * @mbg.generated
     */
    public void setMail(String mail) {
        this.mail = mail == null ? null : mail.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column bs_custom.Site
     *
     * @return the value of bs_custom.Site
     *
     * @mbg.generated
     */
    public String getSite() {
        return site;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column bs_custom.Site
     *
     * @param site the value for bs_custom.Site
     *
     * @mbg.generated
     */
    public void setSite(String site) {
        this.site = site == null ? null : site.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column bs_custom.Fax
     *
     * @return the value of bs_custom.Fax
     *
     * @mbg.generated
     */
    public String getFax() {
        return fax;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column bs_custom.Fax
     *
     * @param fax the value for bs_custom.Fax
     *
     * @mbg.generated
     */
    public void setFax(String fax) {
        this.fax = fax == null ? null : fax.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column bs_custom.Remark
     *
     * @return the value of bs_custom.Remark
     *
     * @mbg.generated
     */
    public String getRemark() {
        return remark;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column bs_custom.Remark
     *
     * @param remark the value for bs_custom.Remark
     *
     * @mbg.generated
     */
    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column bs_custom.OperatorId
     *
     * @return the value of bs_custom.OperatorId
     *
     * @mbg.generated
     */
    public Integer getOperatorId() {
        return operatorId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column bs_custom.OperatorId
     *
     * @param operatorId the value for bs_custom.OperatorId
     *
     * @mbg.generated
     */
    public void setOperatorId(Integer operatorId) {
        this.operatorId = operatorId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column bs_custom.OperatorName
     *
     * @return the value of bs_custom.OperatorName
     *
     * @mbg.generated
     */
    public String getOperatorName() {
        return operatorName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column bs_custom.OperatorName
     *
     * @param operatorName the value for bs_custom.OperatorName
     *
     * @mbg.generated
     */
    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName == null ? null : operatorName.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column bs_custom.SalesManId
     *
     * @return the value of bs_custom.SalesManId
     *
     * @mbg.generated
     */
    public Integer getSalesManId() {
        return salesManId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column bs_custom.SalesManId
     *
     * @param salesManId the value for bs_custom.SalesManId
     *
     * @mbg.generated
     */
    public void setSalesManId(Integer salesManId) {
        this.salesManId = salesManId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column bs_custom.SalesManName
     *
     * @return the value of bs_custom.SalesManName
     *
     * @mbg.generated
     */
    public String getSalesManName() {
        return salesManName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column bs_custom.SalesManName
     *
     * @param salesManName the value for bs_custom.SalesManName
     *
     * @mbg.generated
     */
    public void setSalesManName(String salesManName) {
        this.salesManName = salesManName == null ? null : salesManName.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column bs_custom.AreaId
     *
     * @return the value of bs_custom.AreaId
     *
     * @mbg.generated
     */
    public Integer getAreaId() {
        return areaId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column bs_custom.AreaId
     *
     * @param areaId the value for bs_custom.AreaId
     *
     * @mbg.generated
     */
    public void setAreaId(Integer areaId) {
        this.areaId = areaId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column bs_custom.AreaCode
     *
     * @return the value of bs_custom.AreaCode
     *
     * @mbg.generated
     */
    public String getAreaCode() {
        return areaCode;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column bs_custom.AreaCode
     *
     * @param areaCode the value for bs_custom.AreaCode
     *
     * @mbg.generated
     */
    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode == null ? null : areaCode.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column bs_custom.AreaName
     *
     * @return the value of bs_custom.AreaName
     *
     * @mbg.generated
     */
    public String getAreaName() {
        return areaName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column bs_custom.AreaName
     *
     * @param areaName the value for bs_custom.AreaName
     *
     * @mbg.generated
     */
    public void setAreaName(String areaName) {
        this.areaName = areaName == null ? null : areaName.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column bs_custom.YearBalance
     *
     * @return the value of bs_custom.YearBalance
     *
     * @mbg.generated
     */
    public BigDecimal getYearBalance() {
        return yearBalance;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column bs_custom.YearBalance
     *
     * @param yearBalance the value for bs_custom.YearBalance
     *
     * @mbg.generated
     */
    public void setYearBalance(BigDecimal yearBalance) {
        this.yearBalance = yearBalance;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column bs_custom.PayDay
     *
     * @return the value of bs_custom.PayDay
     *
     * @mbg.generated
     */
    public Integer getPayDay() {
        return payDay;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column bs_custom.PayDay
     *
     * @param payDay the value for bs_custom.PayDay
     *
     * @mbg.generated
     */
    public void setPayDay(Integer payDay) {
        this.payDay = payDay;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column bs_custom.Post
     *
     * @return the value of bs_custom.Post
     *
     * @mbg.generated
     */
    public String getPost() {
        return post;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column bs_custom.Post
     *
     * @param post the value for bs_custom.Post
     *
     * @mbg.generated
     */
    public void setPost(String post) {
        this.post = post == null ? null : post.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column bs_custom.PayMode
     *
     * @return the value of bs_custom.PayMode
     *
     * @mbg.generated
     */
    public String getPayMode() {
        return payMode;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column bs_custom.PayMode
     *
     * @param payMode the value for bs_custom.PayMode
     *
     * @mbg.generated
     */
    public void setPayMode(String payMode) {
        this.payMode = payMode == null ? null : payMode.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column bs_custom.CreateDate
     *
     * @return the value of bs_custom.CreateDate
     *
     * @mbg.generated
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column bs_custom.CreateDate
     *
     * @param createDate the value for bs_custom.CreateDate
     *
     * @mbg.generated
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column bs_custom.MakeBillMan
     *
     * @return the value of bs_custom.MakeBillMan
     *
     * @mbg.generated
     */
    public String getMakeBillMan() {
        return makeBillMan;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column bs_custom.MakeBillMan
     *
     * @param makeBillMan the value for bs_custom.MakeBillMan
     *
     * @mbg.generated
     */
    public void setMakeBillMan(String makeBillMan) {
        this.makeBillMan = makeBillMan == null ? null : makeBillMan.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column bs_custom.ModifyDate
     *
     * @return the value of bs_custom.ModifyDate
     *
     * @mbg.generated
     */
    public Date getModifyDate() {
        return modifyDate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column bs_custom.ModifyDate
     *
     * @param modifyDate the value for bs_custom.ModifyDate
     *
     * @mbg.generated
     */
    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column bs_custom.Modifier
     *
     * @return the value of bs_custom.Modifier
     *
     * @mbg.generated
     */
    public String getModifier() {
        return modifier;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column bs_custom.Modifier
     *
     * @param modifier the value for bs_custom.Modifier
     *
     * @mbg.generated
     */
    public void setModifier(String modifier) {
        this.modifier = modifier == null ? null : modifier.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column bs_custom.DeleteFlag
     *
     * @return the value of bs_custom.DeleteFlag
     *
     * @mbg.generated
     */
    public Boolean getDeleteFlag() {
        return deleteFlag;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column bs_custom.DeleteFlag
     *
     * @param deleteFlag the value for bs_custom.DeleteFlag
     *
     * @mbg.generated
     */
    public void setDeleteFlag(Boolean deleteFlag) {
        this.deleteFlag = deleteFlag;
    }
}