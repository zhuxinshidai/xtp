package cn.xtits.xtp.controller;

import cn.xtits.xtf.common.web.AjaxResult;
import org.joda.time.DateTime;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @fileName: SystemConfig
 * @author: Dan
 * @createDate: 2018-07-11 17:46.
 * @description: 系统配置
 */
@RestController
@RequestMapping("systemConfig")
public class SystemConfig {

    @RequestMapping(value = "getDate")
    public AjaxResult getDate() {

        return new AjaxResult(DateTime.now().toDate());
    }

}
