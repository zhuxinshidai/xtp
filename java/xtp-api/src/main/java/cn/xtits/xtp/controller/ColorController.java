package cn.xtits.xtp.controller;

import cn.xtits.xtf.common.utils.JsonUtil;
import cn.xtits.xtf.common.web.AjaxResult;
import cn.xtits.xtf.common.web.Pagination;
import cn.xtits.xtp.entity.Color;
import cn.xtits.xtp.entity.ColorExample;
import cn.xtits.xtp.service.ColorService;
import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @fileName: ColorController.java
 * @author: Dan
 * @createDate: 2018-02-28 13:43:14
 * @description: 产品颜色基础
 */
@RestController
@RequestMapping("/color_delete")
public class ColorController extends BaseController {

    @Autowired
    private ColorService service;

    @RequestMapping(value = "insertColor")
    public AjaxResult insertColor(
            @RequestParam(value = "data", required = false) String data) {
        Color record = JsonUtil.fromJson(data, Color.class);
        if (exist(record) > 0) {
            return new AjaxResult(-2, "存在相同的编码!");
        }
        //Date dt = getDateNow();
        record.setCreateDate(null);
        record.setMakeBillMan(getUserName());
        record.setModifier(getUserName());
        record.setModifyDate(null);
        record.setDeleteFlag(false);
        service.insert(record);
        return new AjaxResult(record);
    }

    @RequestMapping(value = "deleteColor")
    public AjaxResult deleteColor(
            @RequestParam(value = "id", required = false) int id) {
        Color record = new Color();
        record.setId(id);
        record.setDeleteFlag(true);
        record.setModifier(getUserName());
        record.setModifyDate(null);
        int row = service.updateByPrimaryKeySelective(record);
        return new AjaxResult(row);
    }

    @RequestMapping(value = "updateColor")
    public AjaxResult updateColor(
            @RequestParam(value = "data", required = false) String data) {
        Color record = JsonUtil.fromJson(data, Color.class);
        if (exist(record) > 0) {
            return new AjaxResult(-2, "存在相同的编码!");
        }
        record.setCreateDate(null);
        record.setMakeBillMan(null);
        record.setModifyDate(null);
        record.setModifier(getUserName());
        record.setDeleteFlag(false);
        service.updateByPrimaryKeySelective(record);
        return new AjaxResult(record);
    }

    @RequestMapping(value = "listColor")
    public AjaxResult listColor(
            @RequestParam(value = "code", required = false) String code,
            @RequestParam(value = "name", required = false) String name,
            @RequestParam(value = "pageSize", required = false) Integer pageSize,
            @RequestParam(value = "pageIndex", required = false) Integer pageIndex,
            @RequestParam(value = "orderBy", required = false) String orderBy,
            @RequestParam(value = "startDate", required = false) String startDate,
            @RequestParam(value = "endDate", required = false) String endDate) {
        DateTimeFormatter format = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
        ColorExample example = new ColorExample();
        example.setPageIndex(pageIndex);
        example.setPageSize(pageSize);
        if (StringUtils.isNotBlank(orderBy)) {
            example.setOrderByClause(orderBy);
        }
        ColorExample.Criteria criteria = example.createCriteria();

        criteria.andDeleteFlagEqualTo(false);
        if (StringUtils.isNotBlank(code)) {
            criteria.andCodeLike(code);
        }
        if (StringUtils.isNotBlank(name)) {
            criteria.andNameLike(name);
        }
        if (StringUtils.isNotBlank(startDate)) {
            criteria.andCreateDateGreaterThanOrEqualTo(DateTime.parse(startDate, format).toDate());
        }
        if (StringUtils.isNotBlank(endDate)) {

            criteria.andCreateDateLessThanOrEqualTo(DateTime.parse(endDate, format).toDate());
        }

        List<Color> list = service.listByExample(example);
        Pagination<Color> pList = new Pagination<>(example, list, example.getCount());
        return new AjaxResult(pList);
    }

    @RequestMapping(value = "getColor")
    public AjaxResult getColor(@RequestParam(value = "id", required = false) Integer id) {
        Color res = service.getByPrimaryKey(id);
        return new AjaxResult(res);
    }

    /**
     * 是否存在相同数据
     *
     * @param entity
     * @return 返回相同数据的条数
     */
    private int exist(Color entity) {
        ColorExample example = new ColorExample();
        ColorExample.Criteria criteria = example.createCriteria();
        criteria.andDeleteFlagEqualTo(false);
        if (null != entity.getId() && entity.getId() > 0) {
            criteria.andIdNotEqualTo(entity.getId());
        }
        criteria.andCodeEqualTo(entity.getCode());
        List<Color> list = service.listByExample(example);
        return list.size();
    }

}