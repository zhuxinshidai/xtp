package cn.xtits.xtp.controller;

import cn.xtits.xtf.common.utils.JsonUtil;
import cn.xtits.xtf.common.web.AjaxResult;
import cn.xtits.xtf.common.web.Pagination;
import cn.xtits.xtp.entity.Relation;
import cn.xtits.xtp.entity.RelationExample;
import cn.xtits.xtp.service.RelationService;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @fileName: RelationController.java
 * @author: Dan
 * @createDate: 2018-07-16 18:52:24
 * @description: Relation
 */
@RestController
@RequestMapping("/relation")
public class RelationController extends BaseController {

    @Autowired
    private RelationService relationService;

    //@RequiresPermissions("relation:insert")
    @RequestMapping(value = "insertRelation")
    public AjaxResult insertRelation(
            @RequestParam(value = "data", required = false) String data) {
        Relation record = JsonUtil.fromJson(data, Relation.class);
        //Date dt = getDateNow();
        record.setCreateDate(null);
        record.setMakeBillMan(getUserName());
        record.setModifier(getUserName());
        record.setModifyDate(null);
        record.setDeleteFlag(false);
        relationService.insert(record);
        return new AjaxResult(record);
    }

    //@RequiresPermissions("relation:delete")
    @RequestMapping(value = "deleteRelation")
    public AjaxResult deleteRelation(
            @RequestParam(value = "id", required = false) int id) {
        Relation record = new Relation();
        record.setId(id);
        record.setDeleteFlag(true);
        record.setModifier(getUserName());
        record.setModifyDate(null);
        int row = relationService.updateByPrimaryKeySelective(record);
        return new AjaxResult(row);
    }

    //@RequiresPermissions("relation:update")
    @RequestMapping(value = "updateRelation")
    public AjaxResult updateRelation(
            @RequestParam(value = "data", required = false) String data) {
        Relation record = JsonUtil.fromJson(data, Relation.class);
        record.setCreateDate(null);
        record.setMakeBillMan(null);
        record.setModifyDate(null);
        record.setModifier(getUserName());
        record.setDeleteFlag(false);
        relationService.updateByPrimaryKeySelective(record);
        return new AjaxResult(record);
    }

    @RequestMapping(value = "listRelation")
    public AjaxResult listRelation(
            @RequestParam(value = "relationType", required = false) String relationType,
            @RequestParam(value = "startId", required = false) String startId,
            @RequestParam(value = "startCode", required = false) String startCode,
            @RequestParam(value = "endId", required = false) String endId,
            @RequestParam(value = "endCode", required = false) String endCode,
            @RequestParam(value = "pageSize", required = false) Integer pageSize,
            @RequestParam(value = "pageIndex", required = false) Integer pageIndex,
            @RequestParam(value = "orderBy", required = false) String orderBy,
            @RequestParam(value = "startDate", required = false) String startDate,
            @RequestParam(value = "endDate", required = false) String endDate) {
        DateTimeFormatter format = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
        RelationExample example = new RelationExample();
        example.setPageIndex(pageIndex);
        example.setPageSize(pageSize);
        if (StringUtils.isNotBlank(orderBy)) {
            example.setOrderByClause(orderBy);
        }
        RelationExample.Criteria criteria = example.createCriteria();

        criteria.andDeleteFlagEqualTo(false);
        if (StringUtils.isNotBlank(relationType)) {
            criteria.andRelationTypeLike(relationType);
        }
        if (StringUtils.isNotBlank(startId)) {
            String[] split = startId.split(",");
            if (split.length > 1) {
                List<Integer> startIdList = new ArrayList<>();
                for (String s : split) {
                    startIdList.add(Integer.parseInt(s));
                }
                criteria.andStartIdIn(startIdList);
            } else {
                criteria.andStartIdEqualTo(Integer.parseInt(split[0]));
            }
        }

        if (StringUtils.isNotBlank(startCode)) {
            criteria.andStartCodeLike(startCode);
        }
        if (StringUtils.isNotBlank(endId)) {
            String[] split = endId.split(",");
            if (split.length > 1) {
                List<Integer> endIdList = new ArrayList<>();
                for (String s : split) {
                    endIdList.add(Integer.parseInt(s));
                }
                criteria.andEndIdIn(endIdList);
            } else {
                criteria.andEndIdEqualTo(Integer.parseInt(split[0]));
            }
        }

        if (StringUtils.isNotBlank(endCode)) {
            criteria.andEndCodeLike(endCode);
        }
        if (StringUtils.isNotBlank(startDate)) {
            criteria.andCreateDateGreaterThanOrEqualTo(DateTime.parse(startDate, format).toDate());
        }
        if (StringUtils.isNotBlank(endDate)) {

            criteria.andCreateDateLessThanOrEqualTo(DateTime.parse(endDate, format).toDate());
        }

        List<Relation> list = relationService.listByExample(example);
        Pagination<Relation> pList = new Pagination<>(example, list, example.getCount());
        return new AjaxResult(pList);
    }

    @RequestMapping(value = "getRelation")
    public AjaxResult getRelation(
            @RequestParam(value = "id", required = false) Integer id) {
        Relation res = relationService.getByPrimaryKey(id);
        return new AjaxResult(res);
    }


}