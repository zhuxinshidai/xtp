package cn.xtits.xtp.service.impl;
import cn.xtits.xtp.entity.Area;
import cn.xtits.xtp.entity.AreaExample;
import cn.xtits.xtp.mapper.base.AreaMapper;
import cn.xtits.xtp.service.AreaService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Dan on 2018-02-28 01:53:21
 */
@Service
public class AreaServiceImpl implements AreaService {

    @Resource
    private AreaMapper areaMapper;

    @Override
    public int deleteByPrimaryKey(Integer id) {
        return areaMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(Area record) {
        return areaMapper.insertSelective(record);
    }

    @Override
    public List<Area> listByExample(AreaExample example) {
        PageHelper.startPage(example.getPageIndex().intValue(), example.getPageSize().intValue());
        Page page = (Page) areaMapper.selectByExample(example);
        example.setCount((int)page.getTotal());
        return page.toPageInfo().getList();
    }

    @Override
    public Area getByPrimaryKey(Integer id) {
        return areaMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKey(Area record) {
        return areaMapper.updateByPrimaryKey(record);
    }
    
    @Override
    public int updateByPrimaryKeySelective(Area record) {
        return areaMapper.updateByPrimaryKeySelective(record);
    }



}