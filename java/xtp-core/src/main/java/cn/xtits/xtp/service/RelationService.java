package cn.xtits.xtp.service;

import cn.xtits.xtp.entity.Relation;
import cn.xtits.xtp.entity.RelationExample;

import java.util.List;

/**
 * Created by Generator 2018-07-16 06:52:24
 */
public interface RelationService {

    int deleteByPrimaryKey(Integer id);

    int insert(Relation record);

    List<Relation> listByExample(RelationExample example);

    Relation getByPrimaryKey(Integer id);

    int updateByPrimaryKey(Relation record);

    int updateByPrimaryKeySelective(Relation record);

}