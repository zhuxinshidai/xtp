package cn.xtits.xtp.service;
import cn.xtits.xtp.entity.MeasureUnit;
import cn.xtits.xtp.entity.MeasureUnitExample;
import java.util.List;

/**
 * Created by Dan on 2018-02-28 01:53:21
 */
public interface MeasureUnitService {

    int deleteByPrimaryKey(Integer id);

    int insert(MeasureUnit record);

    List<MeasureUnit> listByExample(MeasureUnitExample example);

    MeasureUnit getByPrimaryKey(Integer id);

    int updateByPrimaryKey(MeasureUnit record);

    int updateByPrimaryKeySelective(MeasureUnit record);

}