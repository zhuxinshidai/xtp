package cn.xtits.xtp.service.impl;

import cn.xtits.xtp.entity.Custom;
import cn.xtits.xtp.entity.CustomExample;
import cn.xtits.xtp.mapper.base.CustomMapper;
import cn.xtits.xtp.service.CustomService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Dan on 2017-12-08 10:37:10
 */
@Service
public class CustomServiceImpl implements CustomService {

    @Resource
    private CustomMapper mapper;


    @Override
    public int deleteByPrimaryKey(Integer id) {
        return mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(Custom record) {
        return mapper.insertSelective(record);
    }

    @Override
    public List<Custom> listByExample(CustomExample example) {
        PageHelper.startPage(example.getPageIndex().intValue(), example.getPageSize().intValue());
        Page page = (Page) mapper.selectByExample(example);
        example.setCount((int)page.getTotal());
        return page.toPageInfo().getList();
    }

    @Override
    public Custom getByPrimaryKey(Integer id) {
        return mapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKey(Custom record) {
        return mapper.updateByPrimaryKey(record);
    }
    
    @Override
    public int updateByPrimaryKeySelective(Custom record) {
        return mapper.updateByPrimaryKeySelective(record);
    }
}