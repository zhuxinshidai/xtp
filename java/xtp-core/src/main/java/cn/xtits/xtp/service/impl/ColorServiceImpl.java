package cn.xtits.xtp.service.impl;
import cn.xtits.xtp.entity.Color;
import cn.xtits.xtp.entity.ColorExample;
import cn.xtits.xtp.mapper.base.ColorMapper;
import cn.xtits.xtp.service.ColorService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Dan on 2018-02-28 01:53:21
 */
@Service
public class ColorServiceImpl implements ColorService {

    @Resource
    private ColorMapper colorMapper;

    @Override
    public int deleteByPrimaryKey(Integer id) {
        return colorMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(Color record) {
        return colorMapper.insertSelective(record);
    }

    @Override
    public List<Color> listByExample(ColorExample example) {
        PageHelper.startPage(example.getPageIndex().intValue(), example.getPageSize().intValue());
        Page page = (Page) colorMapper.selectByExample(example);
        example.setCount((int)page.getTotal());
        return page.toPageInfo().getList();
    }

    @Override
    public Color getByPrimaryKey(Integer id) {
        return colorMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKey(Color record) {
        return colorMapper.updateByPrimaryKey(record);
    }
    
    @Override
    public int updateByPrimaryKeySelective(Color record) {
        return colorMapper.updateByPrimaryKeySelective(record);
    }



}