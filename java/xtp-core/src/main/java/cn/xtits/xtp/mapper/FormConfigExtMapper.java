package cn.xtits.xtp.mapper;

import cn.xtits.xtp.entity.FormConfig;

import java.util.List;
import java.util.Map;

/**
 * @fileName: FormConfigExtMapper
 * @author: Dan
 * @createDate: 2018-05-11 14:14.
 * @description:
 */
public interface FormConfigExtMapper {

    Integer listFormKeyByFormConfigCount(Map<String, Object> map);

    List<FormConfig> listFormKeyByFormConfig(Map<String, Object> map);

}
