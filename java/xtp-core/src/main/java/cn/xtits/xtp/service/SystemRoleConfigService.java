package cn.xtits.xtp.service;
import cn.xtits.xtp.entity.SystemRoleConfig;
import cn.xtits.xtp.entity.SystemRoleConfigExample;
import java.util.List;

/**
 * Created by Dan on 2018-03-22 10:34:08
 */
public interface SystemRoleConfigService {

    int deleteByPrimaryKey(Integer id);

    int insert(SystemRoleConfig record);

    List<SystemRoleConfig> listByExample(SystemRoleConfigExample example);

    SystemRoleConfig getByPrimaryKey(Integer id);

    int updateByPrimaryKey(SystemRoleConfig record);

    int updateByPrimaryKeySelective(SystemRoleConfig record);

}