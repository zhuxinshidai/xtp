
package cn.xtits.xtp.service.impl;

import cn.xtits.xtp.entity.MenuOperation;
import cn.xtits.xtp.entity.MenuOperationExample;
import cn.xtits.xtp.mapper.base.MenuOperationMapper;
import cn.xtits.xtp.service.MenuOperationService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by ShengHaiJiang on 2017/3/7.
 */
@Service
public class MenuOperationServiceImpl implements MenuOperationService {

    @Resource
    private MenuOperationMapper menuOperationMapper;


    @Override
    public int deleteByPrimaryKey(Integer ID) {
        return menuOperationMapper.deleteByPrimaryKey(ID);
    }

    @Override
    public int insert(MenuOperation record) {
        return menuOperationMapper.insert(record);
    }

    @Override
    public List<MenuOperation> listByExample(MenuOperationExample example) {
        PageHelper.startPage(example.getPageIndex().intValue(), example.getPageSize().intValue());
        Page page = (Page) menuOperationMapper.selectByExample(example);
        example.setCount((int) page.getTotal());
        return page.toPageInfo().getList();
    }

    @Override
    public MenuOperation getByPrimaryKey(Integer ID) {
        return menuOperationMapper.selectByPrimaryKey(ID);
    }

    @Override
    public int updateByPrimaryKey(MenuOperation record) {
        return menuOperationMapper.updateByPrimaryKey(record);
    }

    @Override
    public int updateByPrimaryKeySelective(MenuOperation record) {
        return menuOperationMapper.updateByPrimaryKeySelective(record);
    }
}