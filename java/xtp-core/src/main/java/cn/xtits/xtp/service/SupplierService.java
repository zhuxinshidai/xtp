package cn.xtits.xtp.service;

import cn.xtits.xtp.entity.Supplier;
import cn.xtits.xtp.entity.SupplierExample;
import java.util.List;

/**
 * Created by 
 */
public interface SupplierService {

    int deleteByPrimaryKey(Integer id);

    int insert(Supplier record);

    List<Supplier> listByExample(SupplierExample example);

    Supplier getByPrimaryKey(Integer id);

    int updateByPrimaryKey(Supplier record);
    
    int updateByPrimaryKeySelective(Supplier record);
}