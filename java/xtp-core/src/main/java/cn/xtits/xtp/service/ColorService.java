package cn.xtits.xtp.service;
import cn.xtits.xtp.entity.Color;
import cn.xtits.xtp.entity.ColorExample;
import java.util.List;

/**
 * Created by Dan on 2018-02-28 01:53:21
 */
public interface ColorService {

    int deleteByPrimaryKey(Integer id);

    int insert(Color record);

    List<Color> listByExample(ColorExample example);

    Color getByPrimaryKey(Integer id);

    int updateByPrimaryKey(Color record);

    int updateByPrimaryKeySelective(Color record);

}