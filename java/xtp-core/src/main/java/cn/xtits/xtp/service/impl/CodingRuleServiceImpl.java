package cn.xtits.xtp.service.impl;

import cn.xtits.xtp.entity.CodingRule;
import cn.xtits.xtp.entity.CodingRuleExample;
import cn.xtits.xtp.enums.CodingRuleTypeEnum;
import cn.xtits.xtp.mapper.base.CodingRuleMapper;
import cn.xtits.xtp.service.CodingRuleService;
import cn.xtits.xtf.common.exception.XTException;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Dan on 2017/8/22.
 */
@Service
public class CodingRuleServiceImpl implements CodingRuleService {
    @Resource
    private CodingRuleMapper mapper;

    @Override
    public Map<String, Object> getAssemblyCoding(String key) {
        CodingRuleExample codingRuleExample = new CodingRuleExample();
        codingRuleExample.setPageSize(Integer.MAX_VALUE);
        codingRuleExample.setOrderByClause("Sort Asc");
        CodingRuleExample.Criteria codingRuleExampleCriteria = codingRuleExample.createCriteria();
        codingRuleExampleCriteria.andDeleteFlagEqualTo(false);
        codingRuleExampleCriteria.andKeyEqualTo(key);
        List<CodingRule> codingRuleList = mapper.selectByExample(codingRuleExample);
        if (codingRuleList.size() == 0) {
            throw new XTException(key + ":编码规则不存在");
        }
        // 固定字段
        List<String> fixedList = new ArrayList<>();
        Map<String, Object> map = new HashMap<>();
        for (CodingRule codingRule : codingRuleList) {
            if (codingRule.getType() == null) {
                continue;
            }
            if (codingRule.getType().equals(CodingRuleTypeEnum.FIXED_FIELD.type)) {
                if (StringUtils.isNotBlank(codingRule.getInitValue())) {
                    fixedList.add(codingRule.getInitValue());
                }
            } else if (codingRule.getType().equals(CodingRuleTypeEnum.DATE_FIELD.type)) {
                if (StringUtils.isNotBlank(codingRule.getFormat())) {
                    map.put(CodingRuleTypeEnum.DATE_FIELD.key, codingRule.getFormat());
                }
            } else if (codingRule.getType().equals(CodingRuleTypeEnum.IDENTITY_FIELD.type)) {
                map.put(CodingRuleTypeEnum.IDENTITY_FIELD.key, codingRule);
            }
        }
        if (fixedList.size() > 0) {
            StringBuffer spliceStr = new StringBuffer();
            for (String s : fixedList) {
                spliceStr.append(s);
            }
            map.put(CodingRuleTypeEnum.FIXED_FIELD.key, spliceStr.toString());
        }
        return map;
    }

    @Override
    public int deleteByPrimaryKey(Integer id) {
        return mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(CodingRule record) {
        return mapper.insertSelective(record);
    }

    @Override
    public List<CodingRule> listByExample(CodingRuleExample example) {
        PageHelper.startPage(example.getPageIndex().intValue(), example.getPageSize().intValue());
        Page page = (Page) mapper.selectByExample(example);
        example.setCount((int) page.getTotal());
        return page.toPageInfo().getList();
    }

    @Override
    public CodingRule getByPrimaryKey(Integer id) {
        return mapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKey(CodingRule record) {
        return mapper.updateByPrimaryKey(record);
    }

    @Override
    public int updateByPrimaryKeySelective(CodingRule record) {
        return mapper.updateByPrimaryKeySelective(record);
    }
}
