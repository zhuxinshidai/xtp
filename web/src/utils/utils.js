import { Message } from "element-ui";

//判断浏览器是否最小化
const IsMinStatus = () => {
	let isMin = false;
	if (window.outerWidth) {
		isMin = window.outerWidth <= 160 && window.outerHeight <= 30;
	} else {
		isMin = window.screenTop < -30000 && window.screenLeft < -30000;
	}
	return isMin;
};

const CheckExp = {
	isTel(tel) {
		if ((/^1[34578]\d{9}$/.test(tel))) {return true;}
		// if((/^((13[0-9])|(14[5|7])|(15([0-3]|[5-9]))|(18[0,5-9]))\\d{8}$/.test(tel))) return true
		else {return false;}
	},
	isQQ(qq) {
		const re = /[1-9][0-9]{4,}/;
		if (!re.test(qq)) {return false;}
		return true;
	}
};

const GetExitData = function(params, api) {
	return new Promise(function(resolve, reject) {
		let isEmpty = true, id = "";
		api(params).then(({res, list}) => {
			if (res.data.data.count !== 0) {
				isEmpty = false;
				id = list[0].id;
			}
			resolve({isEmpty, id});
		});
	});
};

/*获取用链接传过来的参数：传参数名即可获取*/
const UrlGet = {
	getQueryString(name) {
		const reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
		const r = window.location.search.substr(1).match(reg);
		if (r !== null) {return unescape(r[2]);} return null;
	},
	getRequest() {
		const reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"),
			params = window.location.search || (window.location.href.indexOf("?") !== -1 ? window.location.href.substring(window.location.href.indexOf("?"), window.location.href.length) : ""),
			r = params.substr(1).match(reg), select = 2;
		if (r !== null) {
			return unescape(r[select]);
		} else {
			return null;
		}
	}
};


const MessageBox = {
	codeMessage(code, message, fn) {
		return new Promise((resolve, reject) => {
			const errorCode = [{code: 1, msg: "成功"}, {code: 0, msg: "逻辑错误"}, {code: -1, msg: "服务器错误"}, {code: -2, msg: "记录已存在"}, {code: -3, msg: "存在关联数据"}];
			// SystemConfigAPI.getSysConfigList({}).then((res) => {
			//     if(res.data.code==1) Object.assign(errorCode,res.data.data)
			//         let message='';
			//         message=errorCode.find(element => element.code==code).msg;
			//         resolve({message});
			// })
			let message = "";
			message = errorCode.find((element) => element.code === code).msg;
			resolve({message});
		});
	},
	messageBox(res) {
		this.codeMessage(res.data.code).then(({message}) => {
			message = (res.data.msg && "msg" in res.data) ? res.data.msg : message;
			Message({
				message: message,
				type: "error"
			});
		});
	}
};

const ParamsSelect = {
    getAddSelectParams: (row, list, need, field) => {
      const fieldName = Object.keys(field)[0], // 所需要的数据，搜索的数据，需要获取的字段{需要的字段名：数据里面的字段}，匹配字段(在所需要中数据里面能找到){值的字段名：匹配的字段名}
        childrenFun = function(item) {
          if (item[field[fieldName]] === row[fieldName]) {
            Object.keys(need).map((keyItem) => {
              row[keyItem] = item[need[keyItem]];
            });
            return false;
          }
          if (item.children) {
            item.children.map((child) => {
              childrenFun(child);
            });
          }
        };
      list.map((element) => {
        childrenFun(element);
      });
    }
};

const UrlParamUtil = {
	getUrlParam: (selectName, str) => {
		str = str ? str : location.href;
		let name, value;
		//取得整个地址栏
		let num = str.indexOf("?");
		str = str.substr(num + 1); //取得所有参数   stringvar.substr(start [, length ]
		const arr = str.split("&"); //各个参数放到数组里
		const _arrValue = {};
		for (let i = 0; i < arr.length; i++) {
			num = arr[i].indexOf("=");
			if (num > 0) {
				name = arr[i].substring(0, num);
				value = arr[i].substr(num + 1);
				if (value.indexOf("#") > -1) {
					const number = value.indexOf("#");
					value = value.substring(0, number);
				}
				//对象
				_arrValue[name] = value;
				//数组
				//_arrValue.push({[name]: value})
			}
		}
		if (selectName) {
			return _arrValue[selectName] ? _arrValue[selectName] : null;
		}
		return _arrValue;
	},
	GetQueryString: (name) => {
		const reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
		const params = window.location.search || (window.location.href.indexOf("?") !== -1 ? window.location.href.substring(window.location.href.indexOf("?"), window.location.href.length) : "");
		const r = params.substr(1).match(reg);
		if (r !== null) {
			return unescape(r[2]);
		}
		return null;
	}
};

export {
	IsMinStatus,
	CheckExp,
	GetExitData,
	UrlGet,
	MessageBox,
	ParamsSelect,
	UrlParamUtil
}
