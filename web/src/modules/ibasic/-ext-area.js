import {AreaAPI} from "./base";
import {IBASIC} from "../api";

Object.assign(AreaAPI, {
  listAreaTree(params) {
    return new Promise((resolve) => {
      IBASIC.area.listAreaTree(params).then(({data, res}) => {
        const childrenFun = function (item) {
          item.label = item.name;
          if (item.children) {
            item.children.map((child) => {
              child.label = child.name;
              childrenFun(child);
            });
            if (item.children.length === 0) {
              delete item.children;
            }
          }
        };
        let list = [];
        if (data.code === 1) {
          list = data.data;
        }
        list.map((element) => {
          childrenFun(element);
        });
        resolve({data, list, res});
      });
    });
  }
});
export {
  AreaAPI
};
