import { IBASIC } from "../../api";
import { Message } from "element-ui";

const MeasureUnit = function() {
  return {
    id: 0,
    name: null
  };
},
MeasureUnitAPI = {
  measureUnit: new MeasureUnit(),
  get: () => MeasureUnitAPI.measureUnit,
  set: (value) => {
    MeasureUnitAPI.measureUnit = Object.assign(MeasureUnitAPI.measureUnit, value);
  },
  init: () => new MeasureUnit(),
  getMeasureUnit(params) {
    return new Promise((resolve) => {
      IBASIC.measureUnit.getMeasureUnit(params).then(({data, res}) => {
        let item = {};
        if (data.code === 1) {item = data.data;}
        resolve({ data, item, res });
        });
    });
  },
  listMeasureUnit(params) {
    return new Promise((resolve) => {
      IBASIC.measureUnit.listMeasureUnit(params).then(({data, res}) => {
        let list = [];
        if (data.code === 1) {list = data.data.data;}
        resolve({ data, list, res });
        });
    });
  },
  insertMeasureUnit(params) {
    return new Promise((resolve) => {
      IBASIC.measureUnit.insertMeasureUnit(params).then(({data, res}) => {
        if (data.code === 1) {
          Message({
            message: "新增成功",
            type: "success"
          });
        }
        resolve({data, res});
        });
    });
  },
  updateMeasureUnit(params) {
    return new Promise((resolve) => {
      IBASIC.measureUnit.updateMeasureUnit(params).then(({data, res}) => {
        if (data.code === 1) {
          Message({
            message: "修改成功",
            type: "success"
          });
        }
        resolve({data, res});
        });
    });
  },
  deleteMeasureUnit(params) {
    return new Promise((resolve) => {
      IBASIC.measureUnit.deleteMeasureUnit(params).then(({data, res}) => {
        if (data.code === 1) {
          Message({
            message: "删除成功",
            type: "success"
          });
        }
        resolve({data, res});
        });
    });
  }
};

export { MeasureUnitAPI };
