import {IBASIC} from "../../api";
import {Message} from "element-ui";

const Custom = function () {
    return {
      id: 0,
      code: null,
      name: null,
      shortName: null,
      director: null,
      tel: null,
      phone: null,
      address: null,
      taxId: null,
      mail: null,
      site: null,
      fax: null,
      remark: null,
      operatorId: null,
      operatorName: null,
      salesManId: null,
      salesManName: null,
      areaId: null,
      areaCode: null,
      areaName: null,
      yearBalance: null,
      payDay: null,
      post: null,
      payMode: null
    };
  },
  CustomAPI = {
    custom: new Custom(),
    get: () => CustomAPI.custom,
    set: (value) => {
      CustomAPI.custom = Object.assign(CustomAPI.custom, value);
    },
    init: () => new Custom(),
    getCustom(params) {
      return new Promise((resolve) => {
        IBASIC.custom.getCustom(params).then(({data, res}) => {
          let item = {};
          if (data.code === 1) {
            item = data.data;
          }
          resolve({data, item, res});
        });
      });
    },
    listCustom(params) {
      return new Promise((resolve) => {
        IBASIC.custom.listCustom(params).then(({data, res}) => {
          let list = [];
          if (data.code === 1) {
            list = data.data.data;
          }
          resolve({data, list, res});
        });
      });
    },
    insertCustom(params) {
      return new Promise((resolve) => {
        IBASIC.custom.insertCustom(params).then(({data, res}) => {
          if (data.code === 1) {
            Message({
              message: "新增成功",
              type: "success"
            });
          }
          resolve({data, res});
        });
      });
    },
    updateCustom(params) {
      return new Promise((resolve) => {
        IBASIC.custom.updateCustom(params).then(({data, res}) => {
          if (data.code === 1) {
            Message({
              message: "修改成功",
              type: "success"
            });
          }
          resolve({data, res});
        });
      });
    },
    deleteCustom(params) {
      return new Promise((resolve) => {
        IBASIC.custom.deleteCustom(params).then(({data, res}) => {
          if (data.code === 1) {
            Message({
              message: "删除成功",
              type: "success"
            });
          }
          resolve({data, res});
        });
      });
    }
  };

export {CustomAPI};
