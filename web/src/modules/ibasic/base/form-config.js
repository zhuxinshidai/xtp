import {IBASIC} from "../../api";
import {Message} from "element-ui";

const FormConfig = function () {
    return {
      id: 0,
      formKey: null,
      fieldCode: null,
      requiredFlag: null,
      editShowFlag: null,
      editFlag: null,
      editSort: null,
      listSort: null,
      listShowFlag: null,
      remark: null
    };
  },
  FormConfigAPI = {
    formConfig: new FormConfig(),
    get: () => FormConfigAPI.formConfig,
    set: (value) => {
      FormConfigAPI.formConfig = Object.assign(FormConfigAPI.formConfig, value);
    },
    init: () => new FormConfig(),
    getFormConfig(params) {
      return new Promise((resolve) => {
        IBASIC.formConfig.getFormConfig(params).then(({data, res}) => {
          let item = {};
          if (data.code === 1) {
            item = data.data;
          }
          resolve({data, item, res});
        });
      });
    },
    listFormConfig(params) {
      return new Promise((resolve) => {
        IBASIC.formConfig.listFormConfig(params).then(({data, res}) => {
          let list = [];
          if (data.code === 1) {
            list = data.data.data;
          }
          resolve({data, list, res});
        });
      });
    },
    insertFormConfig(params) {
      return new Promise((resolve) => {
        IBASIC.formConfig.insertFormConfig(params).then(({data, res}) => {
          if (data.code === 1) {
            Message({
              message: "新增成功",
              type: "success"
            });
          }
          resolve({data, res});
        });
      });
    },
    updateFormConfig(params) {
      return new Promise((resolve) => {
        IBASIC.formConfig.updateFormConfig(params).then(({data, res}) => {
          if (data.code === 1) {
            Message({
              message: "修改成功",
              type: "success"
            });
          }
          resolve({data, res});
        });
      });
    },
    deleteFormConfig(params) {
      return new Promise((resolve) => {
        IBASIC.formConfig.deleteFormConfig(params).then(({data, res}) => {
          if (data.code === 1) {
            Message({
              message: "删除成功",
              type: "success"
            });
          }
          resolve({data, res});
        });
      });
    }
  };

export {FormConfigAPI};
